<?php

class databaseController extends tpComponent {
	public function run() {
		$section = '';
		if( isset($_GET['section']) ) $section = $_GET['section'];

		$section_method = 'section_' . $section;
		if(empty($section) || !method_exists($this, $section_method)) {
			tpRegistry::redirect('/index.php?module=database&section=import');
		}
		
		$this->$section_method();
	}

	private function section_import()
	{
		$status = false;

		session_start();

		if(isset($_POST['submit'])) {
			$_SESSION['tmpfile'] = $_POST['tmpfile'];
			$_SESSION['firstrow'] = $_POST['firstrow'];
			$_SESSION['row'] = 0;
			$_SESSION['process'] = true;
		}

		if(isset($_SESSION['process'])) {
			require_once BASE_PATH .'core/libs/PHPExcel/IOFactory.php';
	  		require_once BASE_PATH .'core/libs/PHPExcel/Calculation.php';
			require_once BASE_PATH .'core/libs/PHPExcel.php';

			$xlsfile = BASE_PATH . 'tmp/'.$_SESSION['tmpfile'];
			$first_row = strval($_SESSION['firstrow']);

			$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
			$excel2 = $excel2->load($xlsfile);
			$excel2->setActiveSheetIndex(0);

			$current = $_SESSION['row'];
			$start = $first_row;
			$end = $excel2->getActiveSheet()->getHighestRow();

			if($current > 0) {
				$start = $current;
				if($excel2->getActiveSheet()->getHighestRow() > $current + 399) {
					$current = $current + 399;
					$end = $current;
				}
				else {
					$end = $excel2->getActiveSheet()->getHighestRow();
				}
			}
			else {
				if($excel2->getActiveSheet()->getHighestRow() > $start + 399) {
					$end = $start + 399;
					$current = $end;
				}
			}

			$_SESSION['row'] = $current;
			

			$dataArray = $excel2->getActiveSheet()
			    ->rangeToArray(
			        'A'.$start.':F'.$end,     // The worksheet range that we want to retrieve
			        NULL,        // Value that should be returned for empty cells
			        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
			        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
			        FALSE         // Should the array be indexed by cell row and cell column
			    );

			$status = array('processed' => $_SESSION['processed'], 'succeeded' => 0, 'duplicates' => 0, 'ended' => 0, 'max' => $excel2->getActiveSheet()->getHighestRow());

			foreach($dataArray as $row)
			{
				for ($i = 0; $i < strlen($row[0]); $i++){
				    if( ord($row[0][$i]) < 32 || ord($row[0][$i]) > 126 ) {
				    	$row[0][$i] = ' ';
				    }
				}

				$row[0] = $this->db->escapeString(preg_replace("/[[:blank:]]+/"," ", trim(str_replace("`", "'", str_replace('"', "'", $row[0])))));
				$row[2] = $this->db->escapeString(trim(str_replace('-', '/', $row[2])));
				$row[3] = $this->db->escapeString(trim($row[3]));
				$row[4] = floatval(str_replace(",", ".", $row[4]));
				$row[5] = $this->db->escapeString(trim($row[5]));

				$result = $this->db->query("SELECT id FROM plants WHERE name LIKE '%s' AND size LIKE '%s' AND potsize LIKE '%s' AND producer LIKE '%s' LIMIT 1", $row[0], $row[2], $row[3], $row[5]);

				if(!empty($result)) {
					$status['duplicates']++;
				}
				else
				{
					$this->db->update("INSERT INTO plants (name, size, potsize, price, producer) VALUES ('%s', '%s', '%s', %.2f, '%s')", $row[0], $row[2], $row[3], $row[4], $row[5]);
					$status['succeeded']++;
				}

				$status['processed']++;
			}

			$_SESSION['processed'] = $status['processed'];
			$_SESSION['succeeded'] += $status['succeeded'];
			$_SESSION['duplicates'] += $status['duplicates'];

			if($end == $excel2->getActiveSheet()->getHighestRow()) {
				unlink($xlsfile);
				$status['ended'] = 1;
				$status['succeeded'] = $_SESSION['succeeded'];
				$status['duplicates'] = $_SESSION['duplicates'];
				session_unset();
			}
			else {

				$status['redirect'] = "<script>jQuery(document).ready(function (){
    window.location = '".BASE_URL."/index.php?module=database&section=import';
    });
</script>";
			}	
		}

		$this->output->addContent($this->output->database_import(array('import_status' => $status)));
	}

	private function section_deletion()
	{
		$success = 0;
		if(isset($_POST['submit']) && $_POST['producer'] != 'none') {

			$result = $this->db->query("SELECT COUNT(*) as ilosc FROM plants WHERE producer LIKE '%s'", $_POST['producer']);
			$success = $result[0]['ilosc'];

			$this->db->update("DELETE FROM plants WHERE producer LIKE '%s'", $_POST['producer']);
		}

		$data = $this->db->query("SELECT DISTINCT producer FROM plants ORDER BY producer ASC");

		$HTML = '';
		foreach($data as $value) {
			$HTML .= '<option value="'.$value['producer'].'">'.$value['producer'].'</option>';
		}

		$this->output->addContent($this->output->database_deletion(array('options' => $HTML, 'success' => $success)));
	}
}