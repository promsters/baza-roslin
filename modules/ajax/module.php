<?php

class ajaxController extends tpComponent {

	public function run() {
		$this->output->custom_output = true;

		if(!empty($_GET['section']) && method_exists($this, $_GET['section'])) {
			$str = $_GET['section'];
			$this->$str();
		}
	}

	private function dbimport() {
		require_once BASE_PATH .'core/libs/PHPExcel/IOFactory.php';
  		require_once BASE_PATH .'core/libs/PHPExcel/Calculation.php';
		require_once BASE_PATH .'core/libs/PHPExcel.php';

		if(is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$info = pathinfo($_FILES['file']['name']);
			$newname = substr(md5($info['filename']), rand(0, 20), 5) . '.' . $info['extension']; 
			$xlsfile = BASE_PATH . 'tmp/'.$newname;
			move_uploaded_file($_FILES['file']['tmp_name'], $xlsfile);

			$first_row = 1;

			if(isset($_GET['firstisheader'])) $first_row = 2;

			$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
			$excel2 = $excel2->load($xlsfile);
			$excel2->setActiveSheetIndex(0);

			$dataArray = $excel2->getActiveSheet()
			    ->rangeToArray(
			        'A'.$first_row.':F'.($first_row+5),     // The worksheet range that we want to retrieve
			        NULL,        // Value that should be returned for empty cells
			        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
			        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
			        FALSE         // Should the array be indexed by cell row and cell column
			    );


			echo $this->output->database_importCheckFormat(array('firstrow' => $first_row,'filename' => $newname, 'data' => $dataArray));
		}
	}

	private function getsizes() {
		$plant_name = $this->db->escapeString($_POST['plant_name']);

		$response = $this->db->query("SELECT DISTINCT size FROM plants WHERE name LIKE '%s'", $plant_name);

		$HTML = '<select class="custom-select mb-2 mr-sm-2 mb-sm-0">';
		$HTML .= '<option selected>Rozmiar</option>';

		foreach($response as $value) {
			$HTML .= '<option value="'.$value['size'].'">'.$value['size'].'</option>';
		}

		$HTML .= '</select>';

		echo $HTML;
	}

	private function getpotsizes() {
		$plant_name = $this->db->escapeString($_POST['plant_name']);
		$plant_size = $this->db->escapeString($_POST['plant_size']);

		$response = $this->db->query("SELECT DISTINCT potsize FROM plants WHERE name LIKE '%s' AND size LIKE '%s'", $plant_name, $plant_size);

		$HTML = '<select class="custom-select mb-2 mr-sm-2 mb-sm-0">';
		$HTML .= '<option selected>Doniczka</option>';

		foreach($response as $value) {
			$HTML .= '<option value="'.$value['potsize'].'">'.$value['potsize'].'</option>';
		}

		$HTML .= '</select>';

		echo $HTML;
	}

	private function getplants() {
		$plant_name = '';
		$plant_size = '';
		$plant_potsize = '';

		if(isset($_POST['plant_name'])) {
			$plant_name = 'name LIKE \'' . $this->db->escapeString($_POST['plant_name']) . '\'';
		}

		if(isset($_POST['plant_size'])) {
			$plant_size = ' AND size LIKE \'' . $this->db->escapeString($_POST['plant_size']) . '\'';
		}

		if(isset($_POST['plant_potsize'])) {
			$plant_potsize = ' AND potsize LIKE \'' . $this->db->escapeString($_POST['plant_potsize']) . '\'';
		}

		$response = $this->db->query("SELECT * FROM plants WHERE %s%s%s", $plant_name, $plant_size, $plant_potsize);

		$data = array();
		foreach($response as $val) {
			$val['price'] = number_format($val['price'],2);
			$data[$val['id']] = $val;
		}

		echo json_encode($data);
	}

	private function exportplants() {
		$plants = array();
		$in_stmnt = '';
		$i = 0;
		foreach($_POST['plant_amount'] as $key => $value) {
			$i++;
			$plants[intval($key)] = intval($value);
			if($i == 0) $in_stmnt = $key;
			$in_stmnt = $in_stmnt . ',' . $key;
		}


		$data = $this->db->query("SELECT * FROM plants WHERE id IN (%s)", $in_stmnt);

		require_once BASE_PATH .'core/libs/PHPExcel/IOFactory.php';
  		require_once BASE_PATH .'core/libs/PHPExcel/Calculation.php';
		require_once BASE_PATH .'core/libs/PHPExcel.php';

		$excel = new PHPExcel();
		$excel->createSheet(0);
		$excel->setActiveSheetIndex(0);

		$excel->getActiveSheet()->setCellValue('A1', 'Nazwa rośliny')->setCellValue('B1', 'Rozmiar rośliny')->setCellValue('C1', 'Rozmiar doniczki')->setCellValue('D1', 'Szkółka')->setCellValue('E1', 'Cena/szt')->setCellValue('F1', 'Ilość')->setCellValue('G1', 'Cena');

		$row = 2;
		$price_total = 0;
		foreach($data as $value) {
			$excel->getActiveSheet()->setCellValue('A'.$row, $value['name'])
				->setCellValue('B'.$row, $value['size'])
				->setCellValue('C'.$row, $value['potsize'])
				->setCellValue('D'.$row, $value['producer'])
				->setCellValue('E'.$row, number_format($value['price'],2) . ' zł')
				->setCellValue('F'.$row, $plants[$value['id']])
				->setCellValue('G'.$row, number_format($plants[$value['id']]*number_format($value['price'],2),2) . ' zł');

			$price_total += number_format($plants[$value['id']]*number_format($value['price'],2),2);

			$row++;
		}

		$excel->getActiveSheet()->setCellValue('F'.$row, 'Suma całkowita:')->setCellValue('G'.$row, $price_total . ' zł');

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$objWriter->save('output.xlsx');

		if (file_exists('output.xlsx')) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="wycena.xlsx"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize('output.xlsx'));
		    readfile('output.xlsx');
		    exit;
		}
	}

	private function saveproject() {
		if(empty($_POST['plant_amount'])) return;

		$projectname = $_POST['filename'];
		$plants = json_encode($_POST['plant_amount']);

		$data = $this->db->query("SELECT id FROM projects WHERE name LIKE '%s'", $this->db->escapeString($projectname));

		if(!$data) {
			$this->db->update("INSERT INTO projects (name, plants) VALUES ('%s', '%s')", $this->db->escapeString($projectname), $plants);
		}
		else {
			$this->db->update("UPDATE projects SET plants = '%s' WHERE id = %d", $plants, $data[0]['id']);
		}
	}

	private function projectslist() {
		$data = $this->db->query("SELECT id, name FROM projects");
		$html = '';

		if(!is_array($data))
		{
			echo '<tr><td colspan="2">Brak zapisanych projektów</td></tr>';
			return;
		}

		foreach($data as $project) {
			$html .= '<tr>
		      <td style="padding-top: 16px;">'.$project['name'].'</td>
		      <td style="text-align: center;">
		   		<input type="hidden" value="'.$project['id'].'" />
		      	<div class="btn-group" role="group" aria-label="Basic example">
				  <button type="button" class="projectLoad btn btn-sm btn-primary">Wczytaj</button>
				  <button type="button" class="projectRemove btn btn-sm btn-danger">Usuń</button>
				</div>
		      </td>
		    </tr>';
		}

		echo $html;
	}

	private function removeproject() {
		$id = intval($_GET['id']);

		if(is_numeric($id)) {
			$this->db->update("DELETE FROM projects WHERE id = %d", $id);
		}
	}

	private function loadProject() {
		$id = intval($_GET['id']);

		if(is_numeric($id)) {
			$data = $this->db->query("SELECT * FROM projects WHERE id = %d", $id);

			if(!is_array($data)) return;

			$plants_inproj = json_decode($data[0]['plants'], true);
			$plants = array();
			foreach($plants_inproj as $key => $val) {
				$data = $this->db->query("SELECT * FROM plants WHERE id = %d", intval($key));
				if(is_array($data)) {
					$data[0]['amount'] = intval($val);
					$plants[intval($key)] = $data[0];
				}
			}


			echo json_encode($plants);
		}
	}

	private function creatorimport() {
		require_once BASE_PATH .'core/libs/PHPExcel/IOFactory.php';
  		require_once BASE_PATH .'core/libs/PHPExcel/Calculation.php';
		require_once BASE_PATH .'core/libs/PHPExcel.php';

		if(is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$info = pathinfo($_FILES['file']['name']);
			$newname = substr(md5($info['filename']), rand(0, 20), 5) . '.' . $info['extension']; 
			$xlsfile = BASE_PATH . 'tmp/'.$newname;
			move_uploaded_file($_FILES['file']['tmp_name'], $xlsfile);

			$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
			$excel2 = $excel2->load($xlsfile);
			$excel2->setActiveSheetIndex(0);

			$dataArray = $excel2->getActiveSheet()
			    ->rangeToArray(
			        'A2:B'.$excel2->getActiveSheet()->getHighestRow(),     // The worksheet range that we want to retrieve
			        NULL,        // Value that should be returned for empty cells
			        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
			        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
			        FALSE         // Should the array be indexed by cell row and cell column
			    );

			$html = '';

			foreach($dataArray as $row)
			{
				$nazwa = explode(' - ', $row[0]);
				if(sizeof($nazwa) > 1) {
					$nazwa = str_replace(')', '', $nazwa[sizeof($nazwa)-1]);
				}
				else $nazwa = '';

				if(empty($nazwa) || $nazwa == ' ') {
					$nazwa = explode('(', $row[0]);
					$nazwa = str_replace(')', '', $nazwa[1]);
				}

				// teraz musimy poszukac ich w bazie danych
				$options = '';
				$results = array();
				$parts = explode(' ', $nazwa);

				$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s'", $nazwa);
				if($result) {
					$results = array_merge($results, $result);
				}
				else {
					if(sizeof($parts) > 1)
					{
						$part = array_slice($parts, 0, sizeof($parts)-1);
					
						$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s%%'", implode(' ', $part));
						if($result) {
							$results = array_merge($results, $result);
						}
						else
						{
							if(sizeof($parts) > 2)
							{
								$part = array_slice($parts, 0, sizeof($parts)-2);
							
								$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s%%'", implode(' ', $part));
								if($result) {
									$results = array_merge($results, $result);
								}
								else
								{
									if(sizeof($parts) > 3)
									{
										$part = array_slice($parts, 0, sizeof($parts)-3);
									
										$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s%%'", implode(' ', $part));
										if($result) {
											$results = array_merge($results, $result);
										}
										else
										{
											if(sizeof($parts) > 4)
											{
												$part = array_slice($parts, 0, sizeof($parts)-4);
											
												$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s%%'", implode(' ', $part));
												if($result) {
													$results = array_merge($results, $result);
												}
												else
												{
													if(sizeof($parts) > 5)
													{
														$part = array_slice($parts, 0, sizeof($parts)-5);
													
														$result = $this->db->query("SELECT DISTINCT name FROM plants WHERE replace(name, \"'\", \"\") LIKE '%s%%'", implode(' ', $part));
														if($result) {
															$results = array_merge($results, $result);
														}
														else
														{
															
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

				if(!empty($results)) {
					foreach($results as $val) {
						$options .= '<option value="'.$val['name'].'">'.$val['name'].'</option>';
					}
				}

				$html .= '<tr>
							<td>'.$nazwa.'</td>
							<td><select><option>Wybierz</option>'.$options.'</select></td>
				</tr>';
			}

			echo $this->output->creator_import(array('content' => $html));
		}
	}
}