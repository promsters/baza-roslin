<?php

class viewController extends tpComponent {
	public function run()
	{
		if(isset($_POST['submit'])) {
			$this->output->custom_output = true;
			
			$plants = array();
			$in_stmnt = '';
			$i = 0;
			foreach($_POST['plant_amount'] as $key => $value) {
				$plants[intval($key)] = intval($value);
				if($i == 0) $in_stmnt = $key;
				$in_stmnt = $in_stmnt . ',' . $key;
				$i++;
			}


			$data = $this->db->query("SELECT * FROM plants WHERE id IN (%s)", $in_stmnt);

			require_once BASE_PATH .'core/libs/PHPExcel/IOFactory.php';
	  		require_once BASE_PATH .'core/libs/PHPExcel/Calculation.php';
			require_once BASE_PATH .'core/libs/PHPExcel.php';

			$excel = new PHPExcel();
			$excel->createSheet(0);
			$excel->setActiveSheetIndex(0);

			$excel->getActiveSheet()->setCellValue('A1', 'Nazwa rośliny')->setCellValue('B1', 'Rozmiar rośliny')->setCellValue('C1', 'Rozmiar doniczki')->setCellValue('D1', 'Szkółka')->setCellValue('E1', 'Cena/szt')->setCellValue('F1', 'Ilość')->setCellValue('G1', 'Cena');

			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

			$excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);

			
			

			$row = 2;
			$price_total = 0;
			foreach($data as $value) {
				$excel->getActiveSheet()->setCellValue('A'.$row, $value['name'])
					->setCellValue('B'.$row, $value['size'])
					->setCellValue('C'.$row, $value['potsize'])
					->setCellValue('D'.$row, $value['producer'])
					->setCellValue('E'.$row, number_format($value['price'],2) . ' zł')
					->setCellValue('F'.$row, $plants[$value['id']])
					->setCellValue('G'.$row, number_format($plants[$value['id']]*number_format($value['price'],2),2) . ' zł');

				$price_total += number_format($plants[$value['id']]*number_format($value['price'],2),2);

				$row++;
			}
			$row++;
			$excel->getActiveSheet()->setCellValue('F'.$row, 'Suma całkowita:')->setCellValue('G'.$row, number_format($price_total,2) . ' zł');
			$excel->getActiveSheet()->getStyle('F'.$row.':'.'G'.$row)->getFont()->setBold(true);

			$style = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		        )
		    );
			$excel->getActiveSheet()->getStyle("E1:G".$row)->applyFromArray($style);

		    $styleArray = array(
			      'borders' => array(
			          'allborders' => array(
			              'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			  );
			$excel->getActiveSheet()->getStyle("A1:G".($row-2))->applyFromArray($styleArray);

		    

			$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
			$objWriter->save('output.xlsx');

			if (file_exists('output.xlsx')) {
				$name = "wycena.xlsx";
				if(isset($_POST['filename'])) {
					$name = $_POST['filename'] . '.xlsx';
				}

			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename="'.$name.'"');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize('output.xlsx'));
			    readfile('output.xlsx');
			    exit;
			}
		}

		$result = $this->db->query("SELECT DISTINCT name FROM plants");
		$plants = "";

		if(is_array($result)) {
			foreach($result as $key => $value) 
			{
				$plants = $plants . '"' . $value['name'] . '"';

				if($key != sizeof($result)-1) $plants = $plants . ',';
			}
		}

		$this->output->addContent($this->output->view_view(array('plants' => $plants)));
	}
}