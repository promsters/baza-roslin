<?php

class loginController extends tpComponent {
	public function run()
	{
		$error = false;

		if(isset($_POST['submit'])) {
			if(LOGIN_PASSWORD == $_POST['password']) {
				setcookie("LoggedIn", 1, time()+(60*60), "/");
				tpRegistry::redirect('/');
			}
			else
			{
				$error = true;
			}
		}
		$this->output->addHeaderCss($BASE_URL . 'public/assets/css/login.css');
		// no menu
		$this->output->clearContent();

		$this->output->addContent($this->output->login_main(array('error' => $error)));
	}

}