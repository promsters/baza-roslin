<?php

class tpOutput extends tpComponent {
	public $page_title = PAGE_TITLE;
	public $html;
	public $smarty;
	public $custom_output = false;
	private $header_template = 'header';
	private $footer_template = 'footer';
	private $add_to_header = array();

	public function run() {
		parent::__construct();

		require(BASE_PATH . 'core/libs/smarty/Smarty.class.php');
		$this->smarty = new Smarty();
		$this->smarty->assign('page_title', $this->page_title);
		$this->smarty->assign('ASSETS_PATH', BASE_URL . 'public/assets/');
		$this->smarty->assign('BASE_URL', BASE_URL);
		$this->smarty->assign('BASE_PATH', BASE_PATH);
		$this->smarty->assign('MEMBER', $this->member);
	}

	// getting templates using template name as function //
	public function __call($name, $arguments)
    {
        $html = $this->smarty_getTemplate($name, $arguments);

        if(!$html) return "Error: Template " . $template . " does not exist";

        return $html;
    }

    public function setTitle($title) {
    	$this->smarty->assign('page_title', $this->page_title . ' - ' . $title);
    } 

	public function addContent($html) {
		$this->html .= $html;
	}

	public function addHeaderCss($url) {
		array_push($this->add_to_header, '<link rel="stylesheet" type="text/css" href="'.$url.'">');
	}

	public function addHeaderJs($url) {
		array_push($this->add_to_header, '<script type="text/javascript" src="'.$url.'"></script>');
	}

	public function replaceHeader($template) {
		$this->header_template = $template;
	}

	public function replaceFooter($template) {
		$this->footer_template = $template;
	}

	public function clearContent() {
		$this->html = '';
	}


	private function smarty_getTemplate($template, $arguments=array()) {
		$template = str_replace("_", "/", $template);
		if(!file_exists(BASE_PATH . 'public/templates/' . $template . '.tpl')) {
			trigger_error('Missing template file: ' . $template, E_USER_WARNING);
			return false;
		}

		if($arguments && is_array($arguments[0])) {
			foreach($arguments[0] as $k => $v) {
				$this->smarty->assign($k, $v);
			}
		}

		$return = $this->smarty->fetch(BASE_PATH . 'public/templates/' . $template . '.tpl');

		if($arguments && is_array($arguments[0])) {
			foreach($arguments[0] as $k => $v) {
				$this->smarty->clearAssign($k);
			}
		}

		return $return;
	}

	public function output() {
		if($this->custom_output) return "";
		// add header
		$output = $this->smarty_getTemplate($this->header_template, [['add_to_header' => $this->add_to_header]]);

		//insert content
		$output .= $this->html;

		// add footer
		$output .= $this->smarty_getTemplate($this->footer_template);

		return $output;
	}
}