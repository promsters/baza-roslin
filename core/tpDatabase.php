<?php

class tpDatabase {
	private $handle;
	public function __construct($host, $user, $pass, $database) {
		$this->handle = mysqli_connect($host, $user, $pass, $database);
		if (mysqli_connect_errno($this->handle)) {
		    echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}

		$this->update("SET NAMES utf8");
		$this->update("SET CHARSET utf8");
	}

	// returns enumerated array with associative rows
	public function query($query, ...$args) {
		$result = $this->handle->query(vsprintf($query, $args));

		$data = [];
		$rows = 0;

		if($result) {
			while($row = $result->fetch_assoc()) {
				$data[] = $row;
				$rows++;
			}

		}

		return (!empty($data)) ? $data : false;
	}

	// for update query there is no result. for insert query insert id is returned
	public function update($query, ...$args) {
		$str = vsprintf($query, $args);
		$this->handle->query($str);

		return ($this->handle->insert_id == 0) ? false : $this->handle->insert_id;
	}

	public function escapeString($string) {
		return $this->handle->real_escape_string($string);
	}
}