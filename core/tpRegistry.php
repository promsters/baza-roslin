<?php
class tpRegistry {
	public static $output;
	public static $controller;
	public static $db;
	public static $member;
	public static $test = 4;
	public static $redirect = '';

	public static function redirect($url) {
		header("Location: " . $url);
		die();
	}

	public static function redirectAfterOutput($url) {
		tpRegistry::$redirect = $url;
	}

	public static function run() {
		global $MYSQL;
		spl_autoload_register(function ($class_name) {
			if( stristr($class_name, "smarty") === FALSE )
				include BASE_PATH . 'core/' . $class_name . '.php';
		});
		
		self::$db = new tpDatabase($MYSQL['host'], $MYSQL['user'], $MYSQL['pass'], $MYSQL['database']);
		self::$member = new tpMember();
		self::$output = new tpOutput();
		self::$output->run();
		self::$controller = new tpController();
		self::$controller->run();

		// after controller done its job we output html

		echo self::$output->output();

		if(!empty(self::$redirect)) {
			header("Location: " . self::$redirect);
			die();
		}
	}

}