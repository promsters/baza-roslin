<?php

class tpComponent {
	protected $db;
	protected $output;
	protected $controller;
	protected $member;

	public function __construct() {
		$this->db = tpRegistry::$db;
		$this->output = tpRegistry::$output;
		$this->member = tpRegistry::$member;
		$this->controller = tpRegistry::$controller;
	}
}