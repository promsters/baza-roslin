<?php

class tpController extends tpComponent {

	private $MENU = array();

	public function run()
	{
		// first we check if hes logged in
		if(!$this->member->is_logged && $_GET['module'] != 'login') {
			tpRegistry::redirect(BASE_URL . 'logowanie');
			exit();
		}		

		// first we check for existing modules and get their menus
		$this->loadMenus();

		

		$module = STARTUP_MODULE;
		if( isset($_GET['module']) ) $module = $_GET['module'];

		if (!file_exists(BASE_PATH . 'modules/' . $module . '/module.php'))
		{
			tpRegistry::redirect(BASE_URL);
		}

		$this->output->addContent($this->output->menu(array('menus' => $this->MENU, 'module' => $module, 'section' => $_GET['section'])));

		require_once(BASE_PATH . 'modules/' . $module . '/module.php');
		$class_name = $module . 'Controller';

		$module_instance = new $class_name();
		$module_instance->run();

	}

	private function loadMenus()
	{
		$cdir = scandir(BASE_PATH . 'modules'); 
		foreach ($cdir as $key => $value) 
		{
			if (!in_array($value,array(".",".."))) 
			{ 
				$module_dir = BASE_PATH . 'modules/' . $value;

				if (is_dir($module_dir)) 
				{ 
					if (file_exists($module_dir . '/menu.xml'))
					{
						$xml = new SimpleXMLElement(file_get_contents($module_dir . '/menu.xml'));
						if($xml)
						{
							if($xml->getName() == "menus")
							{
								foreach ($xml->children() as $menu)
								{
									if($menu->getName() == "menu") 
									{
										$parent = $this->addMenu($menu, false, $value);

										foreach($menu->children() as $mchilds)
										{
											if($mchilds->getName() == "submenus")
											{
												foreach($mchilds->children() as $submenu)
												{
													if($submenu->getName() == "submenu")
													{
														$this->addMenu($submenu, $parent);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				} 
			} 
		}

		$this->MENU = $this->array_sort($this->MENU, 'disporder');
	}

	private function array_sort($array, $on, $order=SORT_ASC)
	{
	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	            break;
	            case SORT_DESC:
	                arsort($sortable_array);
	            break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            array_push($new_array, $array[$k]);
	        }
	    }

	    return $new_array;
	}

	private function addMenu($xmlnode, $parent=false, $module="")
	{
		$item = array();
		

		foreach($xmlnode->children() as $attr)
		{
			if($attr->getName() != 'submenus') 
			{
				$item[$attr->getName()] = $attr->__toString();
			}
		}

		if($parent === false)
		{
			$item['module'] = $module;
			return array_push($this->MENU, $item)-1;
		}
		else
		{
			if(!is_array($this->MENU[$parent]['submenus'])) $this->MENU[$parent]['submenus'] = array();
			array_push($this->MENU[$parent]['submenus'], $item);
		}
	}
}