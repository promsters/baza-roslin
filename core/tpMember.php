<?php

class tpMember extends tpComponent {
	public $data = [];
	public $is_logged = false;

	function __construct($member_id = 0) {
		parent::__construct();

		if(!$member_id) {
			// check if is logged in and if is load member data
			if(isset($_COOKIE['LoggedIn'])) {
				$this->is_logged = true;
				setcookie("LoggedIn", 1, time()+(60*60*5), "/");
			}
		}
	}
}