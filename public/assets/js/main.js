var Data = {};

function highFunc(item)
{
	return item;
}

function getPlants()
{
	var info = {};

	if(Data.name) info.plant_name = Data.name;
	if(Data.size) info.plant_size = Data.size;
	if(Data.potsize) info.plant_potsize = Data.potsize;

	$.ajax({
	  url: BASE_URL + 'index.php?module=ajax&section=getplants', 
	  type: 'POST',
	  data: info,
	  dataType: 'json'
	}).done(function(response){
	  	
		$('#plantsBlock').slideDown();

		var $tbody = $('#plantsBlock > table > tbody');

		// first we delete not needed rows

		$('#plantsBlock > table > tbody > tr').each(function() {
			if(!response[$(this).attr('data-id')])
			{
				$(this).remove();
			}
		});

		// then we insert new ones
		$.each(response, function(index, element) {
			if($('#plantsRowId_'+element.id).length == 0) {
	    		$tbody.append('<tr id="plantsRowId_'+ element.id +'"> \
			      <td>'+ element.name +'</td> \
			      <td align="center">'+ element.size +'</td> \
			      <td align="center">'+ element.potsize +'</td> \
			      <td align="center">'+ element.price +' zł</td> \
			      <td>'+ element.producer +'</td> \
			      <td><div class="btn-group"><input type="text" class="plant-amount form-control input-number" value="1" style="border-radius: 0;padding: .075rem .2rem;text-align:center;width:35px;"> \
			              <button value="'+element.id+'" type="button" class="addPlant btn-plus btn btn-success btn-number btn-tiny" data-type="plus"> \
			                  <span class="glyphicon glyphicon-plus"></span> \
			              </button></div></td> \
			    </tr>');
    		}
        });


	});
}

function onSelected(item)
{
	Data = {};
	Data.name = item;

	$('#sizeSelect').hide();
	$('#potsizeSelect').hide();

	getPlants();

	$.ajax({
	  url: BASE_URL + 'index.php?module=ajax&section=getsizes', 
	  type: 'POST',
	  data: {plant_name: item}
	}).done(function(response){
	  	$('#sizeSelect').html(response);
	  	$('#sizeSelect').slideDown();
	});
}

$(document).ready(function() {
	var $input = $("#typeahead");
	$input.typeahead({
	  source: plants,
	  minLength: 2,
	  showHintOnFocus: false,
	  autoSelect: true,
	  highlighter: highFunc,
	  afterSelect: onSelected
	});

	$('#sizeSelect').on('change', 'select', function() {
		if($(this).val() == 'Rozmiar') {
			Data.size = null;
			Data.potsize = null;
			$('#potsizeSelect').hide();

			getPlants();
		}
		else
		{
			Data.size = $(this).val();
			Data.potsize = null;

			getPlants();

			$.ajax({
			  url: BASE_URL + 'index.php?module=ajax&section=getpotsizes', 
			  type: 'POST',
			  data: {plant_name: Data.name, plant_size: Data.size}
			}).done(function(response){
			  	$('#potsizeSelect').html(response);
			  	$('#potsizeSelect').slideDown();
			});
		}
	});

	$('#potsizeSelect').on('change', 'select', function() {
		if($(this).val() == 'Doniczka') Data.potsize = null;
		else Data.potsize = $(this).val();

		getPlants();
	});


	// dodawanie
	$('#plantsBlock').on('click', '.addPlant', function() {
		var id = $(this).val();
		var $tr = $(this).closest('tr');

		var amount = $tr.find('.plant-amount').val();
		if( amount < 0 || amount > 100 ) amount = 1;

		$tr.find('.plant-amount').val('1');

		var exists = false;

		$('#plantsBasket').find('tr').each(function() {
			if($(this).attr('id') == 'plantsRowId_'+id) exists = true;
		});

		if(exists) return;

		$('#plantsBasket').find('#plantsSummary').before(basketRowTemplate(id, $tr.find('td:nth-child(1)').html(), $tr.find('td:nth-child(2)').html(), $tr.find('td:nth-child(3)').html(), $tr.find('td:nth-child(5)').html(), $tr.find('td:nth-child(4)').html(), amount));

		$('#plantsBasket').find('#plantsRowId_'+id).find('input').change();

		calculateSummary();

		$('#plantsBasket').slideDown();
		$('#plantsSummary').show();
	});

	// ilość

	$('#plantsBasket').on('click', '.btn-plus', function () {
		var $ilosc = $(this).closest('.plantIlosc').find('input');

		var amount = parseInt($ilosc.val()) + 1;
		if(amount > 100) amount = 100;

		$ilosc.val(amount);
		$ilosc.change();
	});

	$('#plantsBasket').on('click', '.btn-minus', function () {
		var $ilosc = $(this).closest('.plantIlosc').find('input');

		var amount = parseInt($ilosc.val()) - 1;
		if(amount < 0) {
			$(this).tooltip('dispose');
			$(this).closest('tr').remove();

			$('#plantsBasket').find('.btn-minus').tooltip('update');

			calculateSummary();
		}
		else 
		{
			$ilosc.val(amount);
			$ilosc.change();
		}
	});

	$('#plantsBasket').on('change', '.plant-amount', function () {
		var ilosc = parseInt($(this).val());

		if( ilosc < 0 ) $(this).val("0");
		else if( ilosc > 100 ) $(this).val("100");

		ilosc = parseInt($(this).val());

		if(ilosc == 0) {
			$(this).closest('.plantIlosc').find('.btn-minus').tooltip('show');
		}
		else if(ilosc > 0) {
			$(this).closest('.plantIlosc').find('.btn-minus').tooltip('hide');
		}


		var $tr = $(this).closest('tr');
		var priceper = parseFloat($tr.find('td:nth-child(4) > small').html());

		$tr.find('td:nth-child(4)').html((priceper*ilosc).toFixed(2) + ' zł<small class="form-text text-muted" style="font-size:70%;margin-top:0px;">'+$tr.find('td:nth-child(4) > small').html()+'</small>');

		calculateSummary();
	});

	// zapisywanie
	$('#plantsBasket').on('click', '#savePlantsSchema', function () {
		var name = $('input[name=filename]').val();

		if( $('#saveError').css('display') != 'none' ) $('#saveError').animate({width:'toggle'},150);
		if( $('#saveInfo').css('display') != 'none' ) $('#saveInfo').animate({width:'toggle'},150);

		if(name == '') {
			$('#saveError').html('Musisz podać nazwę projektu');
			$('#saveError').animate({width:'toggle'},150).delay(3000).animate({width:'toggle'},150);
		}
		else
		{
			$('#saveInfo').html('<img src="'+BASE_URL+'public/assets/images/spinner.gif" style="width:24px;height:24px;">');
			$('#saveInfo').animate({width:'toggle'},150);

			$.ajax({
			  url: BASE_URL + 'index.php?module=ajax&section=saveproject', 
			  type: 'POST',
			  data: $('#plantsBasket').find('form').serialize()
			}).done(function(response){
				$('#saveInfo').delay(1000).animate({width:'toggle'},150,'swing', function() {$(this).html('Zapisano');}).delay(200).animate({width:'toggle'},150).delay(3000).animate({width:'toggle'},150);
			});
		}
	});

	// wczytywanie projektu
	$('#loadProject').click(function () {
		$('#loadProject').html('<img src="'+BASE_URL+'public/assets/images/spinner.gif" style="width:24px;height:24px;">');

		$.ajax({
			  url: BASE_URL + 'index.php?module=ajax&section=projectslist', 
			  type: 'GET'
		}).done(function(response){
			$('#loadProject').html('Wczytaj projekt');
			$('#projectsModal').find('tbody').html(response);
			$('#projectsModal').modal('show');
		});

		
	});

	// popup z projektami
	$('#projectsModal').on('click', '.projectRemove', function() {
		var id = parseInt($(this).closest('td').find('input').val());

		$.ajax({
			  url: BASE_URL + 'index.php?module=ajax&section=removeproject&id='+id, 
			  type: 'GET'
		});	

		$(this).closest('tr').remove();
	});

	$('#projectsModal').on('click', '.projectLoad', function() {
		var id = parseInt($(this).closest('td').find('input').val());
		$(this).html('<img src="'+BASE_URL+'public/assets/images/spinner.gif" style="width:24px;height:24px;">');
		var clickedProject = this;
		setTimeout(function () {
			$.ajax({
				  url: BASE_URL + 'index.php?module=ajax&section=loadproject&id='+id, 
				  type: 'GET',
				  dataType: 'json'
			}).done(function(response){
				$('#projectsModal').modal('hide');
				$(clickedProject).html('Wczytaj');

				$('#plantsBasket').find('tr').each(function() {
					if($(this).attr('id') != 'plantsSummary' && $(this).attr('id') != 'plantsHeading') $(this).remove();
				});

				$('input[name=filename]').val($(clickedProject).closest('tr').find('td:nth-child(1)').html());

				$.each(response, function(id, element) {
					$('#plantsBasket').find('#plantsSummary').before(basketRowTemplate(id, element.name, element.size, element.potsize, element.producer, (parseInt(element.price)).toFixed(2) + ' zł', element.amount));

					$('#plantsBasket').find('#plantsRowId_'+id).find('input').change();
				});

				calculateSummary();

				$('#plantsBasket').slideDown();
				$('#plantsSummary').show();
			});
		}, 800);
	});
});

function calculateSummary() {
	Data.price = 0;
	var i = 0;
	$('#plantsBasket').find('tr').each(function() {
		var price = parseFloat($(this).find('td:nth-child(4) > small').html());
		var amount = parseInt($(this).find('.plant-amount').val());

		if(price && amount) Data.price = Data.price + price*amount;

		// dodatkowo updateujemy liczbe porzadkowa
		$(this).find('td:nth-child(1)').html(''+i);
		i = i + 1;
	});

	$('#plantsBasket').find('#plantsSummary > td').html((Data.price).toFixed(2) + ' zł');
}

function basketRowTemplate(id, name, rozmiar, doniczka, szkolka, cena, ilosc) {
	return '<tr id="plantsRowId_'+id+'"> \
							<td></td> \
					  		<td>'+name+' \
					  			<small class="form-text text-muted" style="font-size:70%;margin-top:0px;">rozmiar: '+rozmiar+', doniczka: '+doniczka+', szkółka: '+szkolka+'</small> \
					  		</td> \
					  		<td style="width:130px;"> \
					  			<div class="plantIlosc btn-group"> \
						              <button type="button" class="btn-minus btn btn-danger btn-number btn-tiny"  data-type="minus" title="Wciśnij ponownie, aby skasować element z listy"> \
						                <span class="glyphicon glyphicon-minus"></span> \
						              </button> \
						          <input name="plant_amount['+id+']" type="text" class="plant-amount form-control input-number" value="'+ilosc+'" style="border-radius: 0;padding: .075rem .2rem;text-align:center;width:45px;"> \
						              <button type="button" class="btn-plus btn btn-success btn-number btn-tiny" data-type="plus"> \
						                  <span class="glyphicon glyphicon-plus"></span> \
						              </button> \
						        </div> \
					  		</td> \
					  		<td align="center"> \
					  			'+cena+' \
					  			<small class="form-text text-muted" style="font-size:70%;margin-top:0px;">'+cena+'/szt.</small> \
					  		</td> \
					  	</tr>'
}