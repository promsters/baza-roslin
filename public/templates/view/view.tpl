<script type="text/javascript">
	var plants = [{$plants}];
	var BASE_URL = "{$BASE_URL}";
</script>
<script type="text/javascript" src="{$BASE_URL}public/assets/js/main.js"></script>

<div id="projectsModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Zapisane projekty</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>

<div class="col-5">
	<div class="card">
	  <h3 class="card-header">Lista roślin<button id="loadProject" type="button" class="btn btn-dark" style="float:right;">Wczytaj projekt</button></h3>
	  <div class="card-body">
  		<form class="form-inline">
		  <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" autocomplete="off" id="typeahead" placeholder="nazwa łacińska rośliny">

		  <div id="sizeSelect" style="display:none;">
			  <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
			    <option>Rozmiar</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
		  </div>
		  <div id="potsizeSelect" style="display:none;">
			  <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
			    <option>Doniczka</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
		  </div>
		</form>
	  </div>
  	  <div id="plantsBlock" class="card-body" style="display:none;">
	    <table class="table">
		  <thead class="thead-inverse">
		    <tr>
		      <th>Nazwa</th>
		      <th style="text-align:center;width:15%;">Wielkość</th>
		      <th style="text-align:center;width:15%;">Doniczka</th>
		      <th style="text-align:center;width:15%;">Cena</th>
		      <th style="width:15%;">Szkółka</th>
		      <th style="width:15%;"></th>
		    </tr>
		  </thead>
		  <tbody>

		  </tbody>
		</table>
	  </div>
	</div>
</div>

<div id="plantsBasket" class="col-5" style="display:none;">
	<div class="card">
	  <h3 class="card-header">Lista zakupowa</h3>
	  <div class="card-body">
	  	<form method="post">
	  	<table class="table">
		  <thead class="thead-inverse">
		    <tr id="plantsHeading">
		      <th style="text-align:center;width:20px;">#</th>
		      <th>Roślina</th>
		      <th style="text-align:center;width:130px;">Ilość</th>
		      <th style="text-align:center;width:130px;">Cena</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<tr id="plantsSummary" style="display:none;">
			    <th colspan="3" style="text-align:right;">Suma:</th>
			    <td align="center">642.50 zł</td>
			</tr>
		  </tbody>
		</table>
		<div class="form-inline">
			<input name="filename" type="text" placeholder="nazwa pliku" class="form-control mb-2 mr-sm-2 mb-sm-0" />

			<div class="btn-group">
			  <input name="submit" type="submit" class="btn btn-success" value="Eksportuj" />
			  <button type="button" class="btn btn-success" disabled>lub</button>
			  <button id="savePlantsSchema" type="button" class="btn btn-success">Zapisz</button>
			  <div id="saveError" class="alert alert-danger" role="alert" style="display:inline-block;display:none;border-top-left-radius: 0;border-bottom-left-radius: 0;margin-bottom:0;padding: .35rem 0.75rem;overflow: hidden;white-space: nowrap;text-overflow: ellipsis">
				  
			  </div>
			  <div id="saveInfo" class="alert alert-primary" role="alert" style="display:inline-block;display:none;border-top-left-radius: 0;border-bottom-left-radius: 0;margin-bottom:0;padding: .35rem 0.75rem;overflow: hidden;white-space: nowrap;text-overflow: ellipsis">
				  
			  </div>
			</div>
		</div>
		</form>
  	  </div>
	</div>
</div>
