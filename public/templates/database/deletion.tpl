<div class="col-4">
	<div class="card">
	  <h3 class="card-header">Usuwanie rekordów z wybranej szkółki</h3>
	  <div class="card-body">
	  	{if $success > 0}
  		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true">&times;</span>
		  	</button>
		  	<strong>Usuwanie zakończone pomyślnie!</strong> Usuniętych zostało {$success} rekordów.
		</div>
		{/if}
	  	<div class="alert alert-warning" role="alert">
		  <b>Uwaga!</b> Usuniętych rekordów nie będzie można przywrócić.
		</div>

		<form method="post">
		  <div class="form-group">
		    <label for="exampleFormControlSelect1">Wybierz szkółkę</label>
		    <select class="form-control" name="producer">
		      <option value="none">-----</option>
		      {$options}
		    </select>
		  </div>

		  <div class="form-group">
		  	<input name="submit" type="submit" class="btn btn-primary" value="Usuń rekordy"/>
		  </div>
		</form>
	 	</div>
	 </div>
</div>