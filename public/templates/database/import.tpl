<script type="text/javascript">
	$(document).ready(function () {
		$(document).on('change', '#exampleInputFile', function() {
			if( $(this).get(0).files.length > 0) {

				$('#fileUploadBlock').slideUp();
				$('#loadingBlock').slideDown();

				var firstrow = '';
				if($('#firstRowIsHeader').is(':checked')) {
					firstrow = '&firstisheader=1';
				}

				$.ajax({
				  url: '{$BASE_URL}index.php?module=ajax&section=dbimport'+firstrow, 
				  type: 'POST',
				  data: new FormData($('#uploadForm')[0]), // The form with the file inputs.
				  processData: false,
				  contentType: false,
            	  dataType: 'html',
				}).done(function(response){
					$('#loadingBlock').slideUp();
				  	$('#responseBlock').html(response);
				  	$('#responseBlock').slideDown();
				}).fail(function(){
					$('#loadingBlock').slideUp();
				  	$('#fileUploadBlock').slideDown();

				  	alert("Wystąpił nieoczekiwany błąd, spróbuj ponownie.")
				});
			}
		});
	});
</script>

<div class="col-10">
	<div class="card">
	  <h3 class="card-header">Import cenników ze szkółek</h3>
	  <div class="card-body">
	  	{if is_array($import_status)}
	  		{if !empty($import_status['redirect'])}{$import_status['redirect']}{/if}
	  		<div class="alert {if $import_status['ended'] == 1}alert-success{else}alert-warning{/if} alert-dismissible fade show" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			  	{if $import_status['ended'] == 1}
			  		<strong>Import zakończony sukcesem!</strong> Dodanych zostało {$import_status['succeeded']} rekordów, {$import_status['duplicates']} dupilkatów zostało pominiętych.
			  	{else}
			  		<strong>Import w trakcie...</strong> Przetworzono {$import_status['processed']} rekordów z {$import_status['max']} wszystkich.<br>
			  		<small>Nie odświeżaj strony aż do ukończenia procesu</small>
			  	{/if}
			</div>
	  	{/if}

	  	<form id="uploadForm"{if is_array($import_status) && $import_status['ended'] == 0} style="display:none;"{/if}>
	  		<div id="loadingBlock" class="form-group" style="display:none;">
	  			<img src="{$ASSETS_PATH}images/spinner.gif" style="width:32px;height:32px;" />
	  		</div>
	    	<div id="fileUploadBlock" class="form-group">
			    <label for="exampleInputFile">Wybierz plik z danymi</label>
			    <input type="file" accept=".xlsx,.xls" class="form-control-file" name="file" id="exampleInputFile" aria-describedby="fileHelp">
			    <small id="fileHelp" class="form-text text-muted">Dozwolony format to jedynie plik programu Microsoft Excel z rozszerzeniem .xls</small>

			    <input type="checkbox" class="form-control-checkbox" id="firstRowIsHeader" value="1" checked/> <small class="form-text" style="display: inline-block;">Pierwszy wiersz jest nagłówkiem </small>
		  	</div>
	  	</form>

	  	<div id="responseBlock" style="display: none;">

	  	</div>
	  </div>
	</div>
</div>