

<div class="alert alert-info" role="alert">
  <strong>Uwaga!</strong> Sprawdź poprawność dopasowania danych na pierwszych pięciu wierszach
</div>

<table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>Nazwa łacińska</th>
      <th>Parametr wielkości</th>
      <th>Doniczka</th>
      <th>Cena</th>
      <th>Szkółka</th>
    </tr>
  </thead>
  <tbody>
  	{foreach from=$data item=$row}
	    <tr>
	      <td>{$row[0]}</td>
	      <td>{$row[2]}</td>
	      <td>{$row[3]}</td>
	      <td>{$row[4]}</td>
	      <td>{$row[5]}</td>
	    </tr>
    {/foreach}
  </tbody>
</table>

<form method="post" action="index.php?module=database&section=import">
	<input type="hidden" name="tmpfile" value="{$filename}" />
	<input type="hidden" name="firstrow" value="{$firstrow}" />
	<input id="importAll" type="submit" name="submit" class="btn btn-success btn-lg btn-block" style="cursor:pointer;" value="Importuj dane" />
</form>