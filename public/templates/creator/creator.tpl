<script type="text/javascript">
	$(document).ready(function () {
		$(document).on('change', '#exampleInputFile', function() {
			if( $(this).get(0).files.length > 0) {

				$('#fileUploadBlock').slideUp();
				$('#loadingBlock').slideDown();

				$.ajax({
				  url: '{$BASE_URL}index.php?module=ajax&section=creatorimport', 
				  type: 'POST',
				  data: new FormData($('#uploadForm')[0]), // The form with the file inputs.
				  processData: false,
				  contentType: false,
            	  dataType: 'html',
				}).done(function(response){
					$('#loadingBlock').slideUp();
				  	$('#responseBlock').html(response);
				  	$('#responseBlock').slideDown();
				}).fail(function(){
					$('#loadingBlock').slideUp();
				  	$('#fileUploadBlock').slideDown();

				  	alert("Wystąpił nieoczekiwany błąd, spróbuj ponownie.")
				});
			}
		});
	});
</script>

<div class="col-10">
	<div class="card">
	  <h3 class="card-header">Import roślin z projektu</h3>
	  <div class="card-body">
	  	{if is_array($import_status)}
	  		<div class="alert alert-success alert-dismissible fade show" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			  	<strong>Import zakończony sukcesem!</strong> Dodanych zostało {$import_status['succeeded']} rekordów, {$import_status['duplicates']} dupilkatów zostało pominiętych.
			</div>
	  	{/if}
	  	<form id="uploadForm">
	  		<div id="loadingBlock" class="form-group" style="display:none;">
	  			<img src="{$ASSETS_PATH}images/spinner.gif" style="width:32px;height:32px;" />
	  		</div>
	    	<div id="fileUploadBlock" class="form-group">
			    <label for="exampleInputFile">Wybierz plik z danymi</label>
			    <input type="file" accept=".xlsx,.xls" class="form-control-file" name="file" id="exampleInputFile" aria-describedby="fileHelp">
		  	</div>
	  	</form>

	  	<div id="responseBlock" style="display: none;">

	  	</div>
	  </div>
	</div>
</div>