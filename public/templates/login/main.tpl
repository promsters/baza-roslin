<div class = "container">
	<div class="wrapper">
		<form action="" method="post" class="form-signin">
			  <img src="{$ASSETS_PATH}images/logo-g.jpg">
			  <hr class="colorgraph"><br>
			  <div class="form-row">
				  <div class="input-group">
					  <input type="password" class="form-control" name="password" placeholder="Hasło" />     		  
					  {if $error}
						  <div class="invalid-feedback" style="display:block;">
					        Podane hasło jest błędne.
					      </div>
		      		  {/if}
	      		  </div>
      		  </div>
      		  <div class="form-row" style="margin-top:20px;">
	      		  <div class="input-group">
				  	<input class="btn btn-lg btn-secondary btn-block" name="submit" value="Zaloguj" type="submit"/>  			
				  </div>
			  </div>
		</form>			
	</div>
</div>