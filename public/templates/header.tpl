<!DOCTYPE html>
<html lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{$page_title}</title>
	<link rel="shortcut icon" href="{$ASSETS_PATH}images/LOGO-LO-1.png">

	<link rel="stylesheet" type="text/css" href="{$ASSETS_PATH}css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{$ASSETS_PATH}css/glyphicons.css">
    <link rel="stylesheet" type="text/css" href="{$ASSETS_PATH}css/main.css">

    <script src="{$ASSETS_PATH}js/jquery-3.3.1.min.js"></script>

    {if $add_to_header}
	    {foreach from=$add_to_header item=$add}
	    	{$add}
	    {/foreach}
    {/if}
</head>
<body>
	<div class="customContainer container">
		<div class="row">
			

			
