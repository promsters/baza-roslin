<div class="col-2">
	<div class="list-group">
		<h5 class="list-group-item">Menu</h5>
		{foreach from=$menus item=menu}
			{if !is_array($menu['submenus'])}
				<a href="{$BASE_URL}index.php?module={$menu['module']}" class="list-group-item {if $module == $menu['module']}active{else}list-group-item-action{/if}">{$menu['title']}</a>
			{else}
				<a class="list-group-item {if $module == $menu['module']}active{else}list-group-item-action{/if}" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">{$menu['title']}</a>
				<div class="collapse {if $module == $menu['module']}show{/if}" id="collapseExample">
					{foreach from=$menu['submenus'] item=submenu}
						<a href="{$BASE_URL}index.php?module={$menu['module']}&section={$submenu['href']}" class="list-group-item list-group-item-action{if $module == $menu['module'] && $section == $submenu['href']} active-submenu{/if} submenu">{$submenu['title']}</a>
					{/foreach}
				</div>
			{/if}
		{/foreach}
	</div>
</div>